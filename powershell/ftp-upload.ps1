#directory on local machine
$Dir="C:/my-directory"    
 
#ftp server 
$ftp = "" #use a URL including the folder to be uploaded to
$user = "" 
$pass = ""  
 
$webclient = New-Object System.Net.WebClient 
$webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass)  
 
foreach($item in (dir $Dir "*")){  
    "Uploading $item... to $ftp" 
    $uri = New-Object System.Uri($ftp+$item.Name) 
    $webclient.UploadFile($uri, $item.FullName) 
 } 