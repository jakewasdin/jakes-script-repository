﻿#Generates chocolately package from Visual Studio output after building exe for release.
#Will put every exe file from release into a chocolately package. 
#Used for 'portable' packages, does not check dependencies.
#Need to generate a nuspec file and place it in the $lines variable
#Need to put the package name in the $filename variable

mkdir choco\tools\bin
cd bin\Release
Get-ChildItem *.exe -recurse | Copy-Item -destination ..\..\choco\tools\bin
cd ..\..\choco
$filename = "" + ".nuspec"
$full_filename = join-path $pwd $filename;
$stream = [System.IO.StreamWriter] $full_filename
$lines = "Insert XML here"
$stream.WriteLine($lines);
$stream.close();
choco pack .\$filename