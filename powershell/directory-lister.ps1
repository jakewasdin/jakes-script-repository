#Generate HTML page of directories in folder. 
#Change location of folder to list with $lines.
#Change resulting html file location with $stream
#change text before and after with $url_1 and $url_2

$lines = Get-ChildItem C:\internal-folder-to-list | ?{ $_.PSIsContainer } | Select-Object Name
$url_1 = ''; # text to append before
$url_2 = ''; # text to append after
$stream = [System.IO.StreamWriter] "C:\location-to-write-to.html"
for ($i=0; $i -lt $lines.length; $i++) {
  [string]$temp = $lines[$i];
  $a = "<p>" + $url_1  + $temp.Substring(7).Replace("}","") + $url_2 + "</p>";
  $stream.WriteLine($a);
  }

$stream.close();