$projectname = "ProjectName"
$keyword = "Breaking"

# Set process parameters
$pinfo = New-Object System.Diagnostics.ProcessStartInfo
$pinfo.FileName = "git.exe"
$pinfo.Arguments = "log -p -1"
$pinfo.UseShellExecute = $false
$pinfo.CreateNoWindow = $true
$pinfo.RedirectStandardOutput = $true
$pinfo.RedirectStandardError = $true
$pinfo.WorkingDirectory = $pwd

# Start process
$process = New-Object System.Diagnostics.Process
$process.StartInfo = $pinfo

[void]$process.Start() 

# Get output
$stdout = $process.StandardOutput.ReadToEnd()
$stderr = $process.StandardError.ReadToEnd()

$process.WaitForExit()

# Print if error
Write-Host $stderr

# check output for success information, you may want to check stderr if stdout if empty
if ($stdout.Contains(($keyword))) {
  Write-Host "New breaking changes, incrementing minor version number."
  Increment-Minor-Version
                                  
} else {
   Write-Host "No new breaking changes"

}

Function Increment-Minor-Version() 
{
   $data = Get-Content $projectname "\Properties\AssemblyInfo.cs"

   $full_filename = join-path $pwd $projectname "\Properties\AssemblyInfo.cs";
   $version = $data[-2].Split(".");
   $new_version = [int]$version[1] + 1;

   $new_version_string = $version[0] + "." + $new_version + "." + $version[2];

   $data[-2] = $new_version_string;

   $stream = [System.IO.StreamWriter] $full_filename
       Foreach ($i in $data) {
            $stream.WriteLine($i);
       }
       
   $stream.close();

}