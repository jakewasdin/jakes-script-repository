#!/bin/sh

valid_alphanumeric() {
    stripped="$(echo $1 | sed -e 's/[^[:alnum:]]//g')"

    if [ "$stripped" != "$1" ] ; then
	return 1
    else
	return 0
    fi
}

valid_numeric() {
    stripped="$(echo $1 | sed -e 's/[^[:digit:]]//g')"

    if [ "$stripped" != "$1" ] ; then
        return 1
    else 
        return 0
    fi
}
