#!/bin/bash

# use to remove files from git repository (including history) that are specified in .gitignore file

cat .gitignore | while read line
do
	if [[ $line == *"#"* ]]
	then
		echo #skip
	else
		git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch '"$line" --prune-empty --tag-name-filter cat -- --all
	fi
done

# this last line should be run to actually commit the changes. BE CAREFUL and make sure it works on your local machine first.
#git add . && git commit -m 'removing old code' && git push origin --force --all